package ua.com.danit;

import java.util.Objects;
import java.util.Scanner;

public class Homework3 {
  public static void main(String[] args) {
    String[][] schedule = new String[7][2];
    schedule[0][0] = "Sunday";
    schedule[0][1] = "do home work";
    schedule[1][0] = "Monday";
    schedule[1][1] = "go to courses; watch a film";
    schedule[2][0] = "Tuesday";
    schedule[2][1] = "go to the gym";
    schedule[3][0] = "Wednesday";
    schedule[3][1] = "read a book";
    schedule[4][0] = "Thursday";
    schedule[4][1] = "buy some fruits";
    schedule[5][0] = "Friday";
    schedule[5][1] = "play a football";
    schedule[6][0] = "Saturday";
    schedule[6][1] = "have a break";


    System.out.println("Please input the day of the week");
    Scanner scanner = new Scanner(System.in);
    String day = scanner.nextLine();
    do {
      if (Objects.equals(day, "Monday") || Objects.equals(day, "Tuesday")
        || Objects.equals(day, "Wednesday")
        || Objects.equals(day, "Thursday")
        || Objects.equals(day, "Friday")
        || Objects.equals(day, "Saturday")
        || Objects.equals(day, "Sunday")) {
        showActivities(day);
        System.out.println("Please input the day of the week");
        day = scanner.nextLine();
      } else if (day.equals("exit")) return;
      else {System.out.println("Sorry, I don't understand you, please try again.");
      day = scanner.nextLine();}
    } while (!day.equals("exit"));
  }

  public static void showActivities(String day) {
    switch (day) {
      case ("Monday"):
        System.out.println("go to courses; watch a film");
        break;
      case ("Tuesday"):
        System.out.println("go to the gym");
        break;
      case ("Wednesday"):
        System.out.println("read a book");
        break;
      case ("Thursday"):
        System.out.println("buy some fruits");
        break;
      case ("Friday"):
        System.out.println("play a football");
        break;
      case ("Saturday"):
        System.out.println("have a break");
        break;
      case ("Sunday"):
        System.out.println("do home work");
        break;
    }
  }
}

