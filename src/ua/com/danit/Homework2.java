package ua.com.danit;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Homework2 {
  public static void main(String[] args) {
    System.out.println("All set. Get ready to rumble!");
    String[][] field = initField();

    printField(field);
    do {
      try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write number po stolbu");
        int number = scanner.nextInt();
        System.out.println("Write number po ryadku");
        int number2 = scanner.nextInt();
        if (number == 2 && number2 == 4) {
          field[2][4] = "x";
          printField(field);
          System.out.println("You have won");
        } else if (number > 5 || number < 1 || number2 > 5 || number2 < 1)
        {
          System.out.println("Incorrect field: Try again!");
        }
        else {
          field[number - 1][number2 - 1] = "*";
          printField(field);
        }
      } catch (Exception e) {
        System.out.println("Invalid type");
      }
    } while (!Objects.equals(field[2][4], "x"));
  }

  public static String[][] initField() {
    String[][] field = new String[5][5];
    for (String[] strings : field) {
      Arrays.fill(strings, "-");
    }
    return field;
  }

  public static void printField(String[][] field) {
    System.out.print("    ");
    for (int i = 0; i < field.length; i++) {
      System.out.print(i + 1 + " | ");
    }
    System.out.println();
    for (int i = 0; i < field.length; i++) {
      System.out.print(i + 1 + " | ");
      for (int j = 0; j < field[i].length; j++) {
        System.out.printf("%s | ", field[i][j]);
      }
      System.out.println();
    }

  }
}
