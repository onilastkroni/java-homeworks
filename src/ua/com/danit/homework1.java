package ua.com.danit;

import java.util.Random;
import java.util.Scanner;

public class homework1 {
  public static void main(String[] args) {
    System.out.println("Write your name");
    Scanner scanner = new Scanner(System.in);
    String name = scanner.nextLine();
    System.out.println("Let the game begin!");
    Random random = new Random();
    int result = random.nextInt(100);
//    System.out.println(result);
    System.out.println("Write your number");
    int number;
    do {
      number = scanner.nextInt();
      if (number > result) {
        System.out.println("Your number is too big. Please, try again.");
      } else if (number < result) {
        System.out.println("Your number is too small. Please, try again.");
      }
    } while (number != result);

    System.out.println("Congratulations " + name + "!");
  }
}
